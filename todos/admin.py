from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.

admin.site.register(TodoList)


class TodoListAdmin:
    list_display = ("name", "created_on")


admin.site.register(TodoItem)


class TodoItemAdmin:
    list_display = (
        "task",
        "due_date",
        "is_completed",
    )
