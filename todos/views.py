from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem

# Create your views here.

# def show_recipe(request, recipe_id):
#     recipe = get_object_or_404(Recipe, id=recipe_id)
#     context = {
#         "recipe_object": recipe,
#     }
#     return render(request, "recipes/detail.html", context)

# def list_recipes(request):
#     recipes = Recipe.objects.all()
#     context = {
#         'recipes': recipes,
#     }
#     return render(request, "recipes/list.html", context)

# def recipe_list(request):
#     recipes = Recipe.objects.all()
#     context = context = {
#         "recipe_list": recipes,
#     }
#     return render(request, "recipes/list.html", context)


def todo_list_list(request):
    todos_lists = TodoList.objects.all()
    context = {
        "todos_lists": todos_lists,
    }
    return render(request, "todos/todos_list_list.html", context)


def todos_list_details(request, todo_list_id):
    todos_list_details = get_object_or_404(TodoList, id=todo_list_id)
    context = {
        "todos_list_details": todos_list_details,
    }
    return render(request, "todos/todo_list_details.html", context)
