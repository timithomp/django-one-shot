from django.urls import path
from todos.views import todo_list_list, todos_list_details


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("todos/<int:todo_list_id>/", todos_list_details, name="todos_list_details")
]
